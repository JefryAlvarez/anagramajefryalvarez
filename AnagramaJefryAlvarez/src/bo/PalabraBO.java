/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import dao.PalabraDAO;
import entities.Palabra;
import java.util.LinkedList;

/**
 *
 * @author Jefry Alvarez
 */
public class PalabraBO {
     public LinkedList<Palabra> buscar() {
        return new PalabraDAO().seleccionar();
    }
     public void activar(Palabra u) {
        if (u.isActivo()) {
            return;
        }
        new PalabraDAO().activarDesactivar(u, true);
    }

    public void desactivar(Palabra u) {
        if (!u.isActivo()) {
            return;
        }
        new PalabraDAO().activarDesactivar(u, false);
    }
    
    public Palabra[] palabras(){
        return new PalabraDAO().comparar();
    }
}
