/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Jefry Alvarez
 */
public class Palabra {
    private int id;
    private String palabra;
    private boolean activo;

    public Palabra() {
    }

    
    public Palabra(int id, String palabra, boolean activo) {
        this.id = id;
        this.palabra = palabra;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return  palabra + "--" + activo;
    }
    
}
