/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import bo.PalabraBO;
import entities.Palabra;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Jefry Alvarez
 */
public class FrmJuego extends javax.swing.JFrame {

    private JFrame parent;
    /**
     * Creates new form FrmJuego
     * @param parent
     */
    public FrmJuego(javax.swing.JFrame parent) {
        this.parent=parent;
        initComponents();
        setLocationRelativeTo(parent);
        lblPalabra.setText(palabraRandom());
    }

    /**
     * Genera una palabra random sacada de un array
     * @param random
     * @return 
     */
    public String palabraRandom(){
        String res="";
        PalabraBO pal = new PalabraBO();
        String[] temp= new String[100];
        ;
        Palabra[] temp2= pal.palabras();
        String temp3="";
        for (Palabra palab : pal.buscar()) {
            System.out.println(palab.getPalabra());
            for (int i = 0; i < temp2.length; i++) {
                temp[i]=palab.getPalabra();
            }
            temp3= temp[(int)(Math.random()*temp.length)];
            
        }
        
            
        
 
                
                
            
                
//                for (int i = 0; i < temp3.length(); i++) {
//                    for (int j = 0; j < temp.length; j++) {
//                        temp[j]=(String.valueOf(temp3.charAt(i)));
//                        if(temp[j]!=null){
//                        res+=temp[random];
//                        break;
//                        }
//                        break;
//                    }
////                   
//                }
            
        
//                    System.out.println(temp2);
                    
        return temp3;
    }
     
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblPalabra = new javax.swing.JLabel();
        txtPalabra = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButton1.setText("Conprobar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(103, 103, 103)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPalabra, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPalabra, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(116, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(lblPalabra, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addComponent(txtPalabra, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 44, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       String palabra=txtPalabra.getText();
       PalabraBO pal = new PalabraBO();
        for (Palabra x : pal.buscar() ) {
            if(x.getPalabra().equalsIgnoreCase(palabra)){
                JOptionPane.showMessageDialog(this, "Acertó");
                break;
            }else{
                JOptionPane.showMessageDialog(this, "Incorrecto");
                break;
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel lblPalabra;
    private javax.swing.JTextField txtPalabra;
    // End of variables declaration//GEN-END:variables
}
