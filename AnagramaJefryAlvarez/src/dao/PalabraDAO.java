/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.Palabra;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Jefry Alvarez
 */
public class PalabraDAO {
    public LinkedList<Palabra> seleccionar() {
        LinkedList<Palabra> palabras = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "SELECT id, palabra, activo\n" +
"	FROM public.palabra";
            PreparedStatement stm = con.prepareStatement(sql);
            
//            System.out.println("puto");
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                palabras.add(cargar(rs));
            }

        } catch (Exception ex) {
            throw new RuntimeException("Favor intente nuevamente");
        }
        return palabras;
    }

    private Palabra cargar(ResultSet rs) throws SQLException {
        Palabra u = new Palabra();
        u.setId(rs.getInt(1));
        u.setPalabra(rs.getString(2));
        u.setActivo(rs.getBoolean(3));
        return u;
    }
    public void activarDesactivar(Palabra u, boolean estado) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "update palabra set activo = ? where id = ? ";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setBoolean(1, estado);
            stm.setInt(2, u.getId());

            stm.executeUpdate();

        } catch (Exception ex) {
            throw new RuntimeException("Favor intente nuevamente");
        }
    }

    public Palabra[] comparar() {
        Palabra[] palabras= new Palabra[100];
        try (Connection con = Conexion.getConexion()) {
            String sql = "SELECT id, palabra, activo\n" +
"	FROM public.palabra";
            PreparedStatement stm = con.prepareStatement(sql);
            
//            System.out.println("puto");
            ResultSet rs = stm.executeQuery();
            Palabra per= new Palabra();
            while (rs.next()) {
                 per= cargar(rs);
            }
                for (int i = 0; i < palabras.length; i++) {
                    palabras[i]=per;
                            
                    
                }

        } catch (Exception ex) {
            throw new RuntimeException("Favor intente nuevamente");
        }
        return palabras;
    }
}
